# README #

01-volume_claim.yml - create persistent storage for FNA Platform files

02-fna_deploy.yaml - attach persistent storage to node, run pod and mount volume

03-fna_service.yml - optional service deployment script for Load Balancer 

